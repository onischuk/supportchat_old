﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Chat.BLL.Interfaces;

namespace Chat.WEB.Controllers
{
    public class HomeController : Controller
    {


        public IWidgetService WidgetService { get;  }

        public HomeController(IWidgetService widgetService)
        {
            WidgetService = widgetService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Download()
        {
            string url = Request.Url.Scheme + "://"+ Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            string token = "Test User"; //dummy name for now
            string filePath = Server.MapPath("~/App_Data/chat/widget.zip");
            WidgetService.AddConfigToWidget(token, url,filePath);
            
            string fileType = "application/zip";

            return File(filePath, fileType, "widget.zip");
        }
    }
}
﻿using Chat.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Chat.BLL.Services
{
    class WidgetService : IWidgetService
    {
        public void AddConfigToWidget(string token, string url, string widgetPath)
        {
            JObject configObject = JObject.FromObject(new {
                url, token                
            });
            
            using (FileStream zipToOpen = new FileStream(widgetPath, FileMode.Open))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    string entry = "config/chat.config.json";
                    archive.GetEntry(entry)?.Delete();                    
                    ZipArchiveEntry configEntry = archive.CreateEntry(entry);                    
                    using (StreamWriter writer = new StreamWriter(configEntry.Open()))
                    {
                        writer.WriteLine(configObject.ToString());  
                    }
                }
            }
           

        }
    }
}

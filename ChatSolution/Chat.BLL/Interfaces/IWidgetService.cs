﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.BLL.Interfaces
{
    public interface IWidgetService
    {
        void AddConfigToWidget(string token, string baseUrl, string widgetPath);
    }
}

import Vue from 'vue'
import vueResource from 'vue-resource';
import App from './chat/Chat.vue'


Vue.use(vueResource);

new Vue({
    el: '#chat',
    render: h => h(App)
});
import { Connection } from "./connection";




export default {

    data: function () {
        return {
            messagelist: [],
            message: "",
            closed: true,
            status: false,
            connection: null,
            userName: "",
            rightSide: ['col-md-offset-2']
        }
    },
    mounted: function () {
        this.connection = new Connection();
        this.connection.status.subscribe((status) => {
            this.status = status;
        });
        this.messagelist = this.connection.messages;
        this.userName = this.connection.username;
        this.connection.disconnect();       
        
    },
    beforeDestroy: function () {
        this.connection.disconnect();
    },
    methods: {
        sendMessage() {            
            this.connection.sendMessage(this.message);
            this.message = "";
        },
        toggleWindow() {            
            if (this.closed && !this.status) {                
                this.connection.connect();
            }
            this.closed = !this.closed;
        },
        close() {            
            if (!this.closed) {
                this.connection.disconnect();                
            }
            this.closed = true;

        }
    },
    computed: {
        toggleIcon() {
            return this.closed ? 'add' : 'remove';
        },
        currentTime() {
            return new Date().toLocaleTimeString('en-US', {
                hour12: false,
                hour: "numeric",
                minute: "numeric"
            });
        }
    }
}
import 'expose-loader?jQuery!jquery';
import $ from 'jquery';
import 'ms-signalr-client';
import Rx from "rxjs/Rx";



export class Connection {
    constructor() {
        //dev 
        this.username = "support";
        //dev
        this.hubConnection = $.hubConnection();
        this.chatHubProxy = this.hubConnection.createHubProxy('ChatHub');

        this.attachServerHandlers();
        this.messages = [];
        this.status = new Rx.BehaviorSubject(false);
    }

    connect() {
        let chat = this.chatHubProxy;
        this.hubConnection.start()
            .done(() => {
                //  replece with token
                this.groupName = "Test User";

                //      

                chat.invoke("Connect", this.username, this.groupName);
            }).fail(() => { console.log('Could not connect'); });

    }

    sendMessage(message) {
        this.chatHubProxy.invoke("Send", this.groupName, this.username, message);
    }

    disconnect() {
        this.hubConnection.stop()
        this.status.next(false);
    }

    attachServerHandlers() {
        this.chatHubProxy.on('BroadcastMessage', (name, message) => {
            console.log(message);
            this.messages.push({ username: name, message: message });
        });

        this.chatHubProxy.on('Connected', (userName, allUsers) => {
            if (allUsers.length) {
                setTimeout(() => {
                    this.status.next(true);
                }, 1500);

            }
            this.username = userName;

        });

        this.chatHubProxy.on('NewUserConnected', (userName) => {
            if (!this.status.getValue()) {
                this.status.next(true);
            }
        });

        this.chatHubProxy.on('UserDisconnected', (userName) => {
            console.log("user " + userName + " has left.")
            this.status.next(false);
        });
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Threading.Tasks;
using Chat.BLL.Models;

using Microsoft.AspNet.SignalR;

namespace Chat
{

    public interface IClient
    {
        void BroadcastMessage(string name, string message);
        void Connected(string name, List<ChatUser> users);

        void NewUserConnected(string userName);

        void UserDisconnected(string userName);
    }


    public class ChatHub : Hub<IClient>
    {
        private static Dictionary<string, List<ChatUser>> UserGroups { get; } = new Dictionary<string, List<ChatUser>>();

        public void Send(string groupName, string name, string message) =>
            Clients.Group(groupName).BroadcastMessage(name, message);



        public void Connect(string userName, string groupName)
        {
            string id = Context.ConnectionId;
            Groups.Add(id, groupName);
            if (!UserGroups.ContainsKey(groupName))
            {

                UserGroups[groupName] = new List<ChatUser>
                {
                   new ChatUser { ConnectionId = id, Name = userName }
                };
            }
            else if (!UserGroups[groupName].Any(x => x.ConnectionId == id))
            {
                UserGroups[groupName].Add(new ChatUser { ConnectionId = id, Name = userName });
                Clients.Group(groupName, id).NewUserConnected(userName);
            }
            var otherUsersInGroup = UserGroups[groupName].Where(user => user.ConnectionId != id).ToList();
            Clients.Caller.Connected(userName, otherUsersInGroup);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Func<ChatUser, bool> predicate = userFromGroup => userFromGroup.ConnectionId == Context.ConnectionId;
            var item = UserGroups.Where(keyValuePair => keyValuePair.Value.Any(predicate)).FirstOrDefault();
            if (!string.IsNullOrEmpty(item.Key))
            {
                string groupName = item.Key;
                ChatUser user = item.Value.Where(predicate).FirstOrDefault();

                UserGroups[groupName].Remove(user);
                if (UserGroups[groupName].Count == 0)
                {
                    UserGroups.Remove(groupName);
                }
                Clients.Group(groupName).UserDisconnected(user.Name);
                Groups.Remove(Context.ConnectionId, groupName);

            }

            return base.OnDisconnected(stopCalled);
        }
    }
}